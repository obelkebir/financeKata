package com.kata.financeKata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FinanceController {
   @Autowired
   FinanceService financeService;


    @GetMapping("/client/value/{id}")
    public Double getClientValue(@PathVariable String id) {
        return financeService.getClient(id).calculateValue();
    }
    
    @GetMapping("/wallet/value/{id}")
    public Double getWalletValue(@PathVariable String id) {
        return financeService.getWallet(id).calculateValue();
    }
    @GetMapping("/client/{id}")
        public Client getClient(@PathVariable String id){
            return financeService.getClient(id);
        }

    @GetMapping("/wallet/{id}")
    public Wallet getWallet(@PathVariable String id){
        return financeService.getWallet(id);
        }
    

 }
    