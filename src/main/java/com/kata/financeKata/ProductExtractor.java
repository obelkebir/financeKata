package com.kata.financeKata;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.opencsv.exceptions.CsvException;

public class ProductExtractor extends Extractor{

    HashMap<String,Product> products;
    HashMap<String,Underlying> underlyings;

    public ProductExtractor(HashMap<String,Underlying> underlyings,String filename){
        this.underlyings = underlyings;
        this.products = new HashMap<>();

        List<String[]> allRows = new ArrayList<String[]>();        
        try {
            allRows = readCSV(filename);
        }
        catch (IOException | CsvException e){
            System.err.println(e.getMessage());
        }

        List<String[]> clearedRows=allRows.stream().filter(row -> cleanEmpty(row,5))
                        .map(row -> dataTreatement(row,4))
                        .filter(row->cleanType(row,4))
                        .collect(Collectors.toList());

        dataCreation(clearedRows);
                        
        }

    
    /** 
     * @param productName
     * @param allRows
     * @return List<String[]>
     */
    public  List<String[]>  getProduct(String productName, List<String[]> allRows){
        return allRows.stream().filter(row->row[1].equals(productName)).collect(Collectors.toList());

    }
    
    /** 
     * @param productRows
     * @return ArrayList<Underlying>
     */
    public ArrayList<Underlying> extractUnderlying(List<String[]> productRows){
        ArrayList<Underlying> productUnderlyings = new ArrayList<>();
        for (String[] row : productRows) {
            System.out.println(row[0]+' '+row[1]+ ' ' +row[2]+' '+row[3]+ ' ' +row[4]);
            productUnderlyings.add(this.underlyings.get(row[2]));
        }
        return productUnderlyings;
    }

    /** 
     * @param productName
     * @param allRows
     * @return List<String[]>
     */
    public  List<String[]>  filterProduct(String productName, List<String[]> allRows){
        return allRows.stream().filter(row->!row[1].equals(productName)).collect(Collectors.toList());
    }
    
    /** 
     * @param clearedRows
     */
    public void dataCreation(List<String[]> clearedRows){
        while(!clearedRows.isEmpty()){
            String productName=clearedRows.get(0)[1];
            List<String[]> productRows = getProduct(productName, clearedRows);
            ArrayList<Underlying> underlyings = extractUnderlying(productRows);
            this.products.put(productName, new Product(underlyings,productName));
            clearedRows=filterProduct(productName, clearedRows);

        }
    }
    
    /** 
     * @return HashMap<String, Product>
     */
    public HashMap<String, Product> getProduct() {
        return products;
    }
    
}
