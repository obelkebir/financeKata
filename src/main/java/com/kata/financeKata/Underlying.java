package com.kata.financeKata;



public class Underlying {

    private String name;
    private Currency currency;
    private double price;
    
    
    public Underlying(Currency currency , double price,String name){
        this.currency=currency;
        this.price=price;
        this.name=name;
    }
    
    /** 
     * @return String
     */
    public String getName() {
        return name;
    }
    
    /** 
     * @return Currency
     */
    public Currency getCurrency() {
        return currency;
    }
    
    /** 
     * @return double
     */
    public double getPrice() {
        return price;
    }
    
    /** 
     * @return double
     */
    public double calculateValue(){
        return currency.getValue() * price;
    }


    
}
