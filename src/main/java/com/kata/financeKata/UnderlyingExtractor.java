package com.kata.financeKata;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.opencsv.exceptions.CsvException;

public class UnderlyingExtractor extends Extractor{

    HashMap<String,Currency> currencys;
    HashMap<String,Underlying> underlyings;

    public UnderlyingExtractor(HashMap<String,Currency> currencys,String filename){
        this.underlyings = new HashMap<>();
        this.currencys = currencys;

        List<String[]> allRows = new ArrayList<String[]>();        
        try {
            allRows = readCSV(filename);
        }
        catch (IOException | CsvException e){
            System.err.println(e.getMessage());
        }

        allRows.stream().filter(row -> cleanEmpty(row,5)).map(row -> dataTreatement(row,4)).filter(row->cleanType(row,4)).forEach(row -> dataCreation(row));
        }

    
    /** 
     * @param row
     */
    public void dataCreation(String[] row){
        System.out.println(row[0]+' '+row[1]+ ' ' +row[2]+' '+row[3]+ ' ' +row[4]);
        String underlyingName;
        double underlyingPrice;
        String currencyName;
        
        underlyingName=row[2];
        underlyingPrice=Double.parseDouble(row[4]);
        currencyName=row[3];

        this.underlyings.put(underlyingName, new Underlying(this.currencys.get(currencyName),underlyingPrice,underlyingName));   
    }

    
    /** 
     * @return HashMap<String, Currency>
     */
    public HashMap<String, Currency> getCurrencys() {
        return currencys;
    }

    
    /** 
     * @return HashMap<String, Underlying>
     */
    public HashMap<String, Underlying> getUnderlyings() {
        return underlyings;
    }
       
}