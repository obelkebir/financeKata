package com.kata.financeKata;
import java.util.Set;

public class Wallet {

    private String name;
    private Set<Product> products;
    

    public Wallet(Set<Product> products,String name){
        this.products= products;
        this.name=name;
    }
    
    /** 
     * @return String
     */
    public String getName() {
        return name;
    }

    
    /** 
     * @return Set<Product>
     */
    public Set<Product> getProducts() {
        return products;
    }
    
    
    /** 
     * @return double
     */
    public double calculateValue(){
        double result=0;
        for(Product product:this.products){
            result += product.calculateValue();
        }
        return result;
    }   
}
