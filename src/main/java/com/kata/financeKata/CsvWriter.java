package com.kata.financeKata;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.opencsv.CSVWriter;

public class CsvWriter {

    CSVWriter writerPortofolio;
    CSVWriter writerClient;
    private final static String PATH_PORTFOLIO_CSV =  "Reporting-portfolio.csv";
    private final static String PATH_CLIENT_CSV = "Reporting-client.csv";
    
    public CsvWriter(){
        File file_portfolio = new File(PATH_PORTFOLIO_CSV);
        File file_client = new File(PATH_CLIENT_CSV);
    try {
        FileWriter outputfilePortofolio = new FileWriter(file_portfolio);
        FileWriter outputfileClient = new FileWriter(file_client);
  
        this.writerPortofolio = new CSVWriter(outputfilePortofolio);
        this.writerClient = new CSVWriter(outputfileClient);
        }
    catch (IOException e) {
        System.err.println(e.getMessage());
        }
    }

    public List<String[]> generateClientValue(HashMap<String,Client> clients){
        List<String[]> clientsValue = new ArrayList<>();
        Set<String> allKeys =clients.keySet();
        for (String key : allKeys) {
            String[] row=new String[2];
            row[0]=key;
            row[1]=Double.toString(clients.get(key).calculateValue());
            System.out.println(row[0]+' '+row[1]);
            clientsValue.add(row);
        }
        return clientsValue;
    }

    public void writeClientsvalue(HashMap<String,Client> clients){
        String[] header = { "Client","Value"};
        List<String[]> allRows =generateClientValue(clients);
        allRows.add(0, header);
        writerClient.writeAll(allRows);
        try {
            writerClient.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public List<String[]> generateWalletValue(HashMap<String,Wallet> wallets){
        List<String[]> walletsValue = new ArrayList<>();
        Set<String> allKeys =wallets.keySet();
        for (String key : allKeys) {
            String[] row=new String[2];
            row[0]=key;
            row[1]=Double.toString(wallets.get(key).calculateValue());
            walletsValue.add(row);
        }
        return walletsValue;
    }

    public void writeWalletsvalue(HashMap<String,Wallet> wallets){
        String[] header = { "Portfolio","Value"};
        List<String[]> allRows =generateWalletValue(wallets);
        allRows.add(0, header);
        writerPortofolio.writeAll(allRows);
        try {
            writerPortofolio.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }


}
