package com.kata.financeKata;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.opencsv.exceptions.CsvException;

public class ClientExtractor extends Extractor{

    HashMap<String,Client> clients;
    HashMap<String,Product> products;

    public ClientExtractor(HashMap<String,Product> products,String filename){
        this.products = products;
        this.clients = new HashMap<>();

        List<String[]> allRows = new ArrayList<String[]>();        
        try {
            allRows = readCSV(filename);
        }
        catch (IOException | CsvException e){
            System.err.println(e.getMessage());
        }

        List<String[]> clearedRows=allRows.stream().filter(row -> cleanEmpty(row,3))
                        .map(row -> dataTreatement(row,2))
                        .filter(row->cleanType(row,2))
                        .collect(Collectors.toList());

        dataCreation(clearedRows);      
        }

    
    /** 
     * @param clientName
     * @param allRows
     * @return List<String[]>
     */
    public  List<String[]>  getClient(String clientName, List<String[]> allRows){
        return allRows.stream().filter(row->row[1].equals(clientName)).collect(Collectors.toList());
    }

    
    /** 
     * @param clientRows
     * @return ArrayList<Product>
     */
    public ArrayList<Product> extractProducts(List<String[]> clientRows){
        ArrayList<Product> clientProducts = new ArrayList<>();
        for (String[] row : clientRows) {
            System.out.println(row[0]+' '+row[1]+ ' ' +row[2]);
            clientProducts.add(this.products.get(row[0]));
        }
        return clientProducts;
    }

    
    /** 
     * @param clientRows
     * @return ArrayList<Integer>
     */
    public ArrayList<Integer> extractQuantitys(List<String[]> clientRows){
        ArrayList<Integer> clientProducts = new ArrayList<>();
        for (String[] row : clientRows) {
            clientProducts.add(Integer.parseInt(row[2]));
        }
        return clientProducts;
    } 
    
    /** 
     * @param clientName
     * @param allRows
     * @return List<String[]>
     */
    public  List<String[]>  filterProduct(String clientName, List<String[]> allRows){
        return allRows.stream().filter(row->!row[1].equals(clientName)).collect(Collectors.toList());
    }

    /** 
     * @param clearedRows
     */
    public void dataCreation(List<String[]> clearedRows){
        while(!clearedRows.isEmpty()){
            String clientName=clearedRows.get(0)[1];
            List<String[]> clientRows = getClient(clientName, clearedRows);
            ArrayList<Product> products = extractProducts(clientRows);
            ArrayList<Integer> productsQuantitys = extractQuantitys(clientRows);
            this.clients.put(clientName, new Client(products, productsQuantitys,clientName));
            clearedRows=filterProduct(clientName, clearedRows);

        }
    }

    /** 
     * @return HashMap<String, Client>
     */
    public HashMap<String, Client> getClient() {
        return this.clients;
    }
    
}
