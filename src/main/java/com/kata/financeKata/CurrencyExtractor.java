package com.kata.financeKata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.opencsv.exceptions.CsvException;


public class CurrencyExtractor extends Extractor{

    HashMap<String,Currency> currencys;

    public CurrencyExtractor(String filename){
        this.currencys= new HashMap<>();

        List<String[]> allRows = new ArrayList<String[]>();
        
        try {
            allRows = readCSV(filename);
        }
        catch (IOException | CsvException e){
            System.err.println(e.getMessage());
        }
        allRows.stream().filter(row -> cleanEmpty(row,3)).map(row -> dataTreatement(row,2)).filter(row->cleanType(row,2)).forEach(row -> dataCreation(row));
        this.currencys.put("EUR", new Currency(1,"EUR"));
        }

    
    /** 
     * @param row
     */
    public void dataCreation(String[] row){
        System.out.println(row[0]+' '+row[1]+ ' ' +row[2]);
        String currencyName;
        double currencyValue;
        switch (row[0]) {
            case "EUR": // A mettre en config 
                currencyName = row[1];
                currencyValue = 1 / (Double.parseDouble(row[2]));
                break;
        
            default:
                currencyName = row[0];
                currencyValue = Double.parseDouble(row[2]);
                break;
        }
        this.currencys.put(currencyName, new Currency(currencyValue,currencyName));   
    }

    
    
    /** 
     * @return HashMap<String, Currency>
     */
    public HashMap<String, Currency> getCurrencys() {
        return currencys;
    }
       
}
