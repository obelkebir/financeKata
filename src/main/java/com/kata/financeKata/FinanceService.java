package com.kata.financeKata;


import java.util.HashMap;

import org.springframework.stereotype.Component;

@Component
public class FinanceService {

    private HashMap<String,Currency> currencys;
    private HashMap<String,Underlying> underlyings;
    private HashMap<String,Product> products;
    private HashMap<String,Client> clients;
    private HashMap<String,Wallet> wallets;
    FinanceService(){
        currencys=new CurrencyExtractor("Forex.csv").getCurrencys();
        underlyings=new UnderlyingExtractor(currencys,"Prices.csv").getUnderlyings();
        products=new ProductExtractor(underlyings,"Prices.csv").getProduct();
        clients=new ClientExtractor(products,"Product.csv").getClient();
        wallets = new WalletExtractor(products,"Prices.csv").getWallet();
        CsvWriter writer=new CsvWriter();
        writer.writeClientsvalue(clients);
        writer.writeWalletsvalue(wallets);
    }

    
    /** 
     * @param key
     * @return Client
     */
    public Client getClient(String key){
        return clients.get(key);
    }
    
    /** 
     * @param key
     * @return Wallet
     */
    public Wallet getWallet(String key){
        return wallets.get(key);
    }

    
    /** 
     * @return HashMap<String, Currency>
     */
    public HashMap<String, Currency> getCurrencys() {
        return currencys;
    }
    
    /** 
     * @return HashMap<String, Underlying>
     */
    public HashMap<String, Underlying> getUnderlyings() {
        return underlyings;
    }
    
    /** 
     * @return HashMap<String, Product>
     */
    public HashMap<String, Product> getProducts() {
        return products;
    }
    
    /** 
     * @return HashMap<String, Client>
     */
    public HashMap<String, Client> getClients() {
        return clients;
    }
    
    /** 
     * @return HashMap<String, Wallet>
     */
    public HashMap<String, Wallet> getWallets() {
        return wallets;
    }
    
}
