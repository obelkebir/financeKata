package com.kata.financeKata;
import java.util.ArrayList;



public class Client {

    private String name;
    private ArrayList<Product> products;
    private ArrayList<Integer> productsQuantitys;
    

    
    public Client(ArrayList<Product> products, ArrayList<Integer> productsQuantitys,String name) {
        this.products=products;
        this.productsQuantitys=productsQuantitys;
        this.name=name;
    }
    
    /** 
     * @return String
     */
    public String getName() {
        return name;
    }
    
    /** 
     * @return ArrayList<Product>
     */
    public ArrayList<Product> getProducts() {
        return products;
    }

    
    /** 
     * @return ArrayList<Integer>
     */
    public ArrayList<Integer> getProductsQuantitys() {
        return productsQuantitys;
    }
    
    /** 
     * @return double
     */
    public double calculateValue(){
        double result = 0;
        for (int i = 0; i < this.products.size(); i++) {
            result += productsQuantitys.get(i)*products.get(i).calculateValue();
        }
        return result;
    }
        
}
