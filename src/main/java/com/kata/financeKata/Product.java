package com.kata.financeKata;
import java.util.ArrayList;

public class Product {

    private String name;
    private ArrayList<Underlying> underlyings;

    
    /** 
     * @return ArrayList<Underlying>
     */
    public ArrayList<Underlying> getUnderlyings() {
        return underlyings;
    }

    public Product(ArrayList<Underlying> underlyings,String name){
        this.underlyings=underlyings;
        this.name=name;
    }

    
    /** 
     * @return String
     */
    public String getName() {
        return name;
    }

    
    /** 
     * @return double
     */
    public double calculateValue(){
        double result = 0 ;
        for(Underlying underlying: this.underlyings){
            result += underlying.calculateValue();
        }
        return result;
    }   
}
