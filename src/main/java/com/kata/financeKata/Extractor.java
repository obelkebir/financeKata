package com.kata.financeKata;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

public class Extractor {


    
    /** 
     * @param strNum
     * @return boolean
     */
    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false; 
        }
        return Pattern.compile("-?\\d+(\\.\\d+)?").matcher(strNum).matches();
        }
    
        
        /** 
         * @param row
         * @param length
         * @return boolean
         */
        public boolean cleanEmpty(String[] row,int length){
            boolean state = row.length == length;
            for (int i = 0; i < length; i++) {
                state = row[i].length()>0 && state;
            }
            return state;
        }
    
        
        /** 
         * @param row
         * @param index
         * @return String[]
         */
        public String[] dataTreatement(String[] row,int index){
            row[index]=row[index].replaceAll(",", ".");
            return row;
        }
        
        /** 
         * @param row
         * @param index
         * @return boolean
         */
        public boolean cleanType(String[] row,int index){
            return isNumeric(row[index]);
        }
    
        
        /** 
         * @param fileCsv
         * @return List<String[]>
         * @throws IOException
         * @throws CsvException
         */
        public List<String[]> readCSV(String fileCsv) throws IOException, CsvException{
            String currentPath = new java.io.File(".").getCanonicalPath();
            System.out.println("Actual path = "+ currentPath);
            String fileName = currentPath.toString()+"/src/main/resources/static/"+fileCsv; // A mettre en config 
            System.out.println("Researched path = "+ fileName);
            CSVReader reader = new CSVReader(new FileReader(fileName));
            return reader.readAll();
        }
    
}
