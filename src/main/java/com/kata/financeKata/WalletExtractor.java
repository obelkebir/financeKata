package com.kata.financeKata;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.opencsv.exceptions.CsvException;

public class WalletExtractor extends Extractor{

    HashMap<String,Wallet> wallets;
    HashMap<String,Product> products;

    public WalletExtractor(HashMap<String,Product> products,String filename){
        this.products = products;
        this.wallets = new HashMap<>();

        List<String[]> allRows = new ArrayList<String[]>();        
        try {
            allRows = readCSV(filename);
        }
        catch (IOException | CsvException e){
            System.err.println(e.getMessage());
        }

        List<String[]> clearedRows=allRows.stream().filter(row -> cleanEmpty(row,5))
                        .map(row -> dataTreatement(row,4))
                        .filter(row->cleanType(row,4))
                        .collect(Collectors.toList());

        dataCreation(clearedRows);      
        }

    public  List<String[]>  getWallet(String walletName, List<String[]> allRows){
        return allRows.stream().filter(row->row[0].equals(walletName)).collect(Collectors.toList());
    }

    public Set<Product> extractProducts(List<String[]> walletRows){
        Set<Product> walletProducts = new HashSet<>();
        for (String[] row : walletRows) {
            System.out.println(row[0]+' '+row[1]+ ' ' +row[2]);
            walletProducts.add(this.products.get(row[1]));
        }

        return walletProducts;
    }

    
    public  List<String[]>  filterProduct(String walletName, List<String[]> allRows){
        return allRows.stream().filter(row->!row[0].equals(walletName)).collect(Collectors.toList());
    }

    public void dataCreation(List<String[]> clearedRows){
        while(!clearedRows.isEmpty()){
            String walletName=clearedRows.get(0)[0];
            List<String[]> walletRows = getWallet(walletName, clearedRows);
            Set<Product> products = extractProducts(walletRows);
            this.wallets.put(walletName, new Wallet(products,walletName));
            clearedRows=filterProduct(walletName, clearedRows);

        }
    }

    public HashMap<String, Wallet> getWallet() {
        return this.wallets;
    }
    
}
