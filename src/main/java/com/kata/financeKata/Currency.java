package com.kata.financeKata;

public class Currency {
    
    private String name;
    private double value;

    public Currency(double value, String name){
        this.value=value;
        this.name=name;
    }
    
    /** 
     * @return String
     */
    public String getName() {
        return name;
    }

    
    /** 
     * @return double
     */
    public double getValue() {
        return value;
}
    
}
