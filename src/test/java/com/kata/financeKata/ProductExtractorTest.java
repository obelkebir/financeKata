package com.kata.financeKata;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ProductExtractorTest {
    private static ProductExtractor productExtractor ;

    @BeforeAll
    public static void initialize(){
        CurrencyExtractor currencyExtractor= new CurrencyExtractor("Forex_mock.csv");
        UnderlyingExtractor underlyingExtractor = new UnderlyingExtractor(currencyExtractor.getCurrencys(),"Prices_mock.csv");
        productExtractor = new ProductExtractor(underlyingExtractor.getUnderlyings(),"Prices_mock.csv");
    }

    @Test
    public void testDataCreation(){
        assert productExtractor.getProduct().size() == 2; 
    }



    
}
