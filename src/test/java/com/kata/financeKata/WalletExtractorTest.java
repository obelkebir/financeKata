package com.kata.financeKata;

import org.junit.jupiter.api.BeforeAll;

public class WalletExtractorTest {

    static WalletExtractor walletExtractor;

    @BeforeAll
    static void initialize(){
        CurrencyExtractor currencys=new CurrencyExtractor("Forex_mock.csv");
        UnderlyingExtractor underlyings=new UnderlyingExtractor(currencys.getCurrencys(),"Prices_mock.csv");
        ProductExtractor products=new ProductExtractor(underlyings.getUnderlyings(),"Prices_mock.csv");
        walletExtractor = new WalletExtractor(products.getProduct(), "Prices_mock.csv");
    }

    public void testDataTreatement(){
        assert walletExtractor.getWallet().size()==1;
    }
    
}
