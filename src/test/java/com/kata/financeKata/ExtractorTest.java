package com.kata.financeKata;

import java.io.IOException;

import com.opencsv.exceptions.CsvException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ExtractorTest {
    static Extractor extractor;
    static String[] tabempty;
    static String[] tabnotempty;
    static String[] tabmisformated;
    static int length;

    @BeforeAll
    public static void initialize(){
        length=3;
        extractor=new Extractor();
        tabempty = new String[3];
        tabempty[0]="";
        tabempty[1]="emptyfirst";
        tabempty[2]="emptyfirst";
        tabnotempty = new String[3];
        tabnotempty[0]="not";
        tabnotempty[1]="empty";
        tabnotempty[2]="all";
        tabmisformated = new String[3];
        tabmisformated[0]="2,5";
        tabmisformated[1]="2,4";
        tabmisformated[2]="2,1";
    }

    @Test
    public void testIsNumeric(){
        assert (!extractor.isNumeric("K9J") && extractor.isNumeric("98.6"));
    }

    @Test
    public void testCleanEmpty(){
        assert (!extractor.cleanEmpty(tabempty, length) && extractor.cleanEmpty(tabnotempty, length));
    }
    @Test
    public void testDataTreatement(){
        assert (extractor.dataTreatement(tabmisformated, 2)[2].equals("2.1"));
    }
    
    @Test 
	void testreadCsv(){
		try {
            assert (extractor.readCSV("Forex.csv").size()>0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvException e) {
            e.printStackTrace();
        }
	}

    
}
