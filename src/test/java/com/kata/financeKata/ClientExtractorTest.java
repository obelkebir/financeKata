package com.kata.financeKata;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ClientExtractorTest {

    private static ClientExtractor clientExtractor;

    @BeforeAll
    static void initialize(){
        CurrencyExtractor currencys=new CurrencyExtractor("Forex_mock.csv");
        UnderlyingExtractor underlyings=new UnderlyingExtractor(currencys.getCurrencys(),"Prices_mock.csv");
        ProductExtractor products=new ProductExtractor(underlyings.getUnderlyings(),"Prices_mock.csv");
        clientExtractor=new ClientExtractor(products.getProduct(),"Product_mock.csv");
    }
    
    @Test
    public void testDataCreation(){
        assert (clientExtractor.getClient().size()==5);
    }
}
