package com.kata.financeKata;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UnderlyingExtractorTest {
    
    private static UnderlyingExtractor underlyingExtractor;

    @BeforeAll
    public static void initialize(){
        CurrencyExtractor currencyExtractor= new CurrencyExtractor("Forex_mock.csv");
        underlyingExtractor = new UnderlyingExtractor(currencyExtractor.getCurrencys(),"Prices_mock.csv");
    }

    @Test
    public void testDataCreation(){
        assert (underlyingExtractor.getUnderlyings().size() == 4);
    }
    
}
