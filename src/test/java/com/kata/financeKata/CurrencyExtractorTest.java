package com.kata.financeKata;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CurrencyExtractorTest {
    static CurrencyExtractor currencyExtractor;

    @BeforeAll
    public static void initialize(){
        currencyExtractor = new CurrencyExtractor("Forex_mock.csv");
    }

    @Test
    public void testDataCreation(){
        assert (currencyExtractor.getCurrencys().size() == 4);
    }
    
}
