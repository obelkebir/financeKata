## Author
*BELKEBIR Omar*

## Running the application locally

* To generate the two requested csv files: 
    Position yourself on the `/financeKata` directory then apply the following command:
    ```./mvnw clean -f pom.xml && ./mvnw compile -f pom.xml && ./mvnw package -f pom.xml```

* To run unit tests:
    Position yourself on the `/financeKata` directory then apply the following command:
    ```./mvnw clean -f pom.xml && ./mvnw compile -f pom.xml && ./mvnw test -f pom.xml```

* To run the webService:
    Position yourself on the `/financeKata` directory then apply the following command:
    ```./mvnw clean -f pom.xml && ./mvnw compile -f pom.xml && ./mvnw package -f pom.xml && java -jar target/financeKata-0.0.1-SNAPSHOT.jar```

## Additional features

The implementation of the project in web service comes with some additional features:

* Consulting the value of a client using the path:
    ```GET /client/value/{clientName}```

* Consulting the value of a portfolio using the path:
    ```GET /wallet/value/{walletName}```
* Consulting the client object and its composition providing the products as well as the values and quantitys ​​using the path:
    ```GET /client/{clientName}```
* Consulting the client object and its composition providing the products as well as the values and quantitys ​​using the path:
    ```GET /wallet/{walletName}```


